FROM node:alpine

ENV NODE_DEV = 'prod'

USER node
WORKDIR /home/node

ADD ./index.js /home/node/
ADD ./package.json /home/node/

RUN npm install --only=production

CMD ["npm", "run", "start"]
