/* eslint-disable no-console */

const fse = require('fs-extra');
const path = require('path');
const checker = require('license-checker');

const appDir = path.resolve(path.join(__dirname, '../../'));
const licensesCheckerSettingsFile = 'licenses-checker.json';
const licensesCheckerSettingsFilePath = path.join(
  appDir,
  licensesCheckerSettingsFile,
);
const licenses = [
  'WTFPL',
  'AFL-2.1',
  'AFL-3.0',
  'APSL-2.0',
  'Apache-1.1',
  'Apache-2.0',
  'Artistic-1.0',
  'Artistic-2.0',
  'BSD-2-Clause',
  'BSD-3-Clause',
  'BSL-1.0',
  'CC-BY-1.0',
  'CC-BY-2.0',
  'CC-BY-2.5',
  'CC-BY-3.0',
  'CC-BY-4.0',
  'CC0-1.0',
  'CDDL-1.0',
  'CDDL-1.1',
  'CPL-1.0',
  'EPL-1.0',
  'FTL',
  'IPL-1.0',
  'ISC',
  'LGPL-2.0',
  'LGPL-2.1',
  'LGPL-3.0',
  'LPL-1.02',
  'MIT',
  'MPL-1.0',
  'MPL-1.1',
  'MPL-2.0',
  'MS-PL',
  'NCSA',
  'OpenSSL',
  'PHP-3.0',
  'Ruby',
  'Unlicense',
  'W3C',
  'Xnet',
  'ZPL-2.0',
  'Zend-2.0',
  'Zlib',
  'libtiff',
  'Public Domain',
  '0BSD',
];

const checkLicenses = (ignoreLicenses = [], ignorePackages = []) => {
  checker.init(
    {
      start: process.cwd(),
      exclude: ignoreLicenses.join(','),
      excludePackages: ignorePackages.join(','),
      production: !!process.env.PRODUCTION_ONLY,
    },
    (err, packages) => {
      let exitCode = 0;
      if (err) {
        console.error(err);
        process.exit(1);
      }

      console.log(packages);
      exitCode = !!Object.keys(packages).length;
      process.exit(exitCode);
    },
  );
};

if (fse.pathExistsSync(licensesCheckerSettingsFilePath)) {
  const settings = fse.readJsonSync(licensesCheckerSettingsFilePath);
  checkLicenses(
    licenses.concat(settings.ignoreLicenses || []),
    settings.ignorePackages,
  );
} else {
  checkLicenses(licenses);
}
